# README

# INFO

https://renov-fauteuil.herokuapp.com/ | https://git.heroku.com/renov-fauteuil.git



Things you may want to cover:

* Ruby version

* System dependencies

  - devise for user authentification
  - image store on Cloudinary via Active Storage and gem image_processing & gem mini_magick
  - mailing with sendgrid

* Configuration

* Database creation

- Postgres

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

• Deploy on Heroku Server
• verify that email sender for sendgrid is authentificate, change default sender in user_mailer.rb, verify URL link for new user

* ...


# email inline css template 
https://github.com/leemunroe/responsive-html-email-template


# Une fois nom de domaine réservé

- changer le lien vers le site dans les emails